package com.tvdinh.entity;

import com.tvdinh.atrributeconverter.LocalDateCryptoConverter;
import com.tvdinh.atrributeconverter.LocalDateTimeCryptoConverter;
import com.tvdinh.atrributeconverter.StringCryptoConverter;
import com.tvdinh.entitylistener.encryption.Encrypted;
import jakarta.persistence.Column;
import jakarta.persistence.Convert;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "employee_test")
public class EmployeeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "employee_id")
    private Long id;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "phone")
    @Encrypted
    private String phone;

    /**
     * This String attribute needs to be encrypted
     */
    @Column(name = "sensitive_data")
    @Convert(converter = StringCryptoConverter.class)
    private String sensitiveData;

    @Column(name = "birth_date")
    @Convert(converter = LocalDateCryptoConverter.class)
    private LocalDate birthDate;

    @Column(name = "creation_date")
    @Convert(converter = LocalDateTimeCryptoConverter.class)
    private LocalDateTime creationDate;
}
