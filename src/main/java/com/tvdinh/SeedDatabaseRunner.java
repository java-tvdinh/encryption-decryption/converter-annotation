package com.tvdinh;

import com.tvdinh.entity.EmployeeEntity;
import com.tvdinh.repository.EmployeeRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class SeedDatabaseRunner implements CommandLineRunner {
    private final EmployeeRepository employeeRepository;

    @Override
    public void run(String... args) throws Exception {
        //add();
        List<EmployeeEntity> employeeEntities = employeeRepository.search("0395725180".toLowerCase());
        //List<EmployeeEntity> employeeEntities = employeeRepository.findAll();
        for (EmployeeEntity e : employeeEntities) {
            log.info("Employee: {}", e);
        }
    }

    private void add() {
        EmployeeEntity e = new EmployeeEntity();
        e.setFirstName("Nguyen Văn");
        e.setLastName("Nam");
        e.setSensitiveData("Nguyen Van Nam");
        e.setPhone("0395725180");
        e.setBirthDate(LocalDate.now());
        e.setCreationDate(LocalDateTime.now());
        employeeRepository.save(e);
    }
}
