package com.tvdinh;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ConverterAnnotationCryptographyApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConverterAnnotationCryptographyApplication.class, args);
	}

}
