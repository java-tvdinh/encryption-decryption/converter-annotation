package com.tvdinh.atrributeconverter;

import jakarta.persistence.Converter;
import org.springframework.util.StringUtils;

import java.time.LocalDate;

import static java.time.format.DateTimeFormatter.ISO_DATE;

@Converter
public class LocalDateCryptoConverter extends AbstractCryptoConverter<LocalDate> {

    public LocalDateCryptoConverter() {
        this(new CipherInitializer());
    }

    public LocalDateCryptoConverter(CipherInitializer cipherInitializer) {
        super(cipherInitializer);
    }

    @Override
    boolean isNotNullOrEmpty(LocalDate attribute) {
        return attribute != null;
    }

    @Override
    LocalDate stringToEntityAttribute(String dbData) {
        return StringUtils.hasLength(dbData) ? LocalDate.parse(dbData, ISO_DATE) : null;
    }

    @Override
    String entityAttributeToString(LocalDate attribute) {
        return attribute == null ? null : attribute.format(ISO_DATE);
    }
}
