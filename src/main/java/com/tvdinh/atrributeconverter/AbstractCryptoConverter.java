package com.tvdinh.atrributeconverter;

import jakarta.persistence.AttributeConverter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.StringUtils;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import static com.tvdinh.atrributeconverter.KeyProperty.DATABASE_ENCRYPTION_KEY;

@Slf4j
abstract class AbstractCryptoConverter<T> implements AttributeConverter<T, String> {

    private final CipherInitializer cipherInitializer;

    protected AbstractCryptoConverter() {
        this(new CipherInitializer());
    }

    protected AbstractCryptoConverter(CipherInitializer cipherInitializer) {
        this.cipherInitializer = cipherInitializer;
    }

    @Override
    public String convertToDatabaseColumn(T attribute) {
        if (StringUtils.hasLength(DATABASE_ENCRYPTION_KEY) && isNotNullOrEmpty(attribute)) {
            try {
                Cipher cipher =
                        cipherInitializer.prepareAndInitCipher(
                                Cipher.ENCRYPT_MODE, DATABASE_ENCRYPTION_KEY);
                return encrypt(cipher, attribute);
            } catch (NoSuchAlgorithmException
                     | InvalidKeyException
                     | InvalidAlgorithmParameterException
                     | BadPaddingException
                     | NoSuchPaddingException
                     | IllegalBlockSizeException e) {
                // throw new RuntimeException(e);
                log.error("Convert to database column error", e);
            }
        }
        return entityAttributeToString(attribute);
    }

    @Override
    public T convertToEntityAttribute(String dbData) {
        if (StringUtils.hasLength(DATABASE_ENCRYPTION_KEY) && StringUtils.hasLength(dbData)) {
            try {
                Cipher cipher =
                        cipherInitializer.prepareAndInitCipher(
                                Cipher.DECRYPT_MODE, DATABASE_ENCRYPTION_KEY);
                return decrypt(cipher, dbData);
            } catch (NoSuchAlgorithmException
                     | InvalidKeyException
                     | InvalidAlgorithmParameterException
                     | BadPaddingException
                     | NoSuchPaddingException
                     | IllegalBlockSizeException e) {
                // throw new RuntimeException(e);
                log.error("Convert to entity attribute error", e);
            }
        }
        return stringToEntityAttribute(dbData);
    }

    abstract boolean isNotNullOrEmpty(T attribute);

    abstract T stringToEntityAttribute(String dbData);

    abstract String entityAttributeToString(T attribute);

    byte[] callCipherDoFinal(Cipher cipher, byte[] bytes)
            throws IllegalBlockSizeException, BadPaddingException {
        return cipher.doFinal(bytes);
    }

    private String encrypt(Cipher cipher, T attribute)
            throws IllegalBlockSizeException, BadPaddingException {
        byte[] bytesToEncrypt = entityAttributeToString(attribute).getBytes();
        byte[] encryptedBytes = callCipherDoFinal(cipher, bytesToEncrypt);
        return Base64.getEncoder().encodeToString(encryptedBytes);
    }

    private T decrypt(Cipher cipher, String dbData)
            throws IllegalBlockSizeException, BadPaddingException {
        byte[] encryptedBytes = Base64.getDecoder().decode(dbData);
        byte[] decryptedBytes = callCipherDoFinal(cipher, encryptedBytes);
        return stringToEntityAttribute(new String(decryptedBytes));
    }
}
