package com.tvdinh.repository;

import com.tvdinh.entity.EmployeeEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface EmployeeRepository extends JpaRepository<EmployeeEntity, Long> {
    @Query("from EmployeeEntity e where lower(e.phone) = :keyword ")
    List<EmployeeEntity> search(String keyword);
}
